# Bienvenido Proyecto de Marcador de Libros y Links
Permite organizar la información de Links y Documentos obtenidos de Internet, permitiendo realizar un filtrado.

# Instalación

- Descargar o clonar el repositorio.
- Generar las dependencias y librerías
```bash
cl.dearc.bookmark:$ npm install
```
- Ejecutar el Servidor Local
```bash
cl.dearc.bookmark:$ php artisan serve
```

## Versión
1.0.0-alpha

## Autor
Daniel Esteban Arce Pérez <arcedaniel@gmail.com>

## Fecha 
Septiembre 2019
